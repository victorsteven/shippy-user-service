package main


import (
	"fmt"
	pb "gitlab.com/victorsteven/shippy-user-service/proto/auth"
	"strings"
	"testing"

	//	"github.com/dgrijalva/jwt-go"
//	"time"
)

var (
	user = &pb.User{
		Id: "abc123",
		Email: "steven@example.com",
	}
)

type MockRepo struct{}

func (repo *MockRepo) GetAll() ([]*pb.User, error) {
	var users []*pb.User
	return users, nil
}

func (repo *MockRepo) Create(user *pb.User) error {
	return nil
}

func (repo *MockRepo) Get(id string) (*pb.User, error) {
	var user *pb.User
	return user, nil
}

func (repo *MockRepo) GetByEmail(email string) (*pb.User, error) {
	var user *pb.User
	return user, nil
}

func newInstance() Authable {
	repo := &MockRepo{}
	return &TokenService{repo}
}

func TestCanCreateToken(t *testing.T) {
	srv := newInstance()
	token, err := srv.Encode(user)
	if err != nil {
		t.Fail()
	}
	if token == "" {
		t.Fail()
	}
	if len(strings.Split(token, ".")) != 3 {
		t.Fail()
	}
}

func TestCanDecodeToken(t *testing.T) {
	srv := newInstance()
	token, err := srv.Encode(user)
	if err != nil {
		t.Fail()
	}
	fmt.Println("the token: ", token)
	claims, err := srv.Decode(token)
	if err != nil {
		t.Fail()
	}
	if claims.User == nil {
		t.Fail()
	}
	if claims.User.Email != "steven@example.com" {
		t.Fail()
	}
}

func TestThrowErrorIfTokenInvalid(t *testing.T) {
	srv := newInstance()
	_, err := srv.Decode("nope.nope.nope")
	if err == nil {
		t.Fail()
	}
}