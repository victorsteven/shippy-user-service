module gitlab.com/victorsteven/shippy-user-service

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.3.5
	github.com/jinzhu/gorm v1.9.12
	github.com/micro/go-micro v1.18.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
)
