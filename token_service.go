package main

import (
	"github.com/dgrijalva/jwt-go"
	pb "gitlab.com/victorsteven/shippy-user-service/proto/auth"
	"time"
)

var (
	key = []byte("jdfjksdjfknsdkjf")
)

//CustomClaims is our custom metadata, which will be hashed and sent as the second segment in our JWT
type CustomClaims struct {
	User *pb.User
	jwt.StandardClaims
}

type Authable interface {
	Decode(token string) (*CustomClaims, error)
	Encode(user *pb.User) (string, error)
}
type TokenService struct {
	repo Repository
}

//Decode a token string into a token object
func (srv *TokenService) Decode(tokenString string) (*CustomClaims, error) {
	//Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
	//Validate the token and return the custom claims
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}

//Encdoe a claims into a JWT
func (srv *TokenService) Encode(user *pb.User) (string, error) {
	expireToken := time.Now().Add(time.Hour * 72).Unix()

	//Create the Claims
	claims := CustomClaims{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer: "shippy.user",
		},
	}
	//Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//Sign token and return
	return token.SignedString(key)
}