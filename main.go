package main

import (
	"github.com/micro/go-micro"
	pb "gitlab.com/victorsteven/shippy-user-service/proto/auth"
	"log"
)

func main() {
	db, err := CreateConnection()
	defer db.Close()

	if err != nil {
		log.Fatalf("could not connect to DB: %v", err)
	}

	db.AutoMigrate(&pb.User{})

	repo := &UserRepository{db}

	tokensService := &TokenService{repo}

	srv := micro.NewService(
		//name of our package
		micro.Name("shippy.user.service"),
	)

	srv.Init()

	//Get instance of the broker using our defaults
	pubsub := srv.Server().Options().Broker

	//Register handler
	pb.RegisterAuthHandler(srv.Server(), &service{repo, tokensService, pubsub})

	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}